<?php

return [
    'password' => false,
    'user_model' => '\App\User',
    'api_host' => env('SSO_AUTH_HOST', 'https://auth.dkbmed.com'),
    'column' => 'sso_id',

//  WARNING: this options will be public visible, so no token\pass\etc here!
    'public' => [
        'redirect' => '/',
        'custom_fields' => [
//            'test' => [
//                'type' => 'text',
//                'label' => 'OK ?',
//                'required' => true,
//                'placeholder' => 'sure',
//                'validation' => 'numeric',
//            ],
//            'more' => [
//                'label' => 'Test Select',
//                'placeholder' => 'Awesome select',
//                'required' => true,
//                'type' => 'select',
//                'options' => [
//                    [
//                        'value' => 'some value',
//                        'label' => 'first'
//                    ], [
//                        'value' => 'some value',
//                        'label' => 'first'
//                    ], [
//                        'value' => 'some value',
//                        'label' => 'first'
//                    ],
//                ],
//            'extra' => [
//                'key' => 'center_extra', // User model should have corresponding mutator
//                'invoke_value' => 294, // on this value input will appear
//                'label' => 'Please specify',
//                'required' => true,
//                'type' => 'text', // only available option for now
//            ]
//            ]
        ],
    ],
];
