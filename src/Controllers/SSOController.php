<?php

namespace DKBmed\SSO\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;

class SSOController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function storeCustomData(Request $request)
    {
        $request->validate(
            collect(config('sso.public.custom_fields'))
                ->mapWithKeys(function ($v, $k) {
                    if (
                        ($v['extra']['invoke_value'] ?? false)
                        && \request()->has($k)
                        && (\request()->get($k) === $v['extra']['invoke_value'])
                        && (($v['extra']['required'] ?? false)
                            || ($v['extra']['validation'] ?? false))
                    ) {
                        return [
                            $k => ($v['required'] ?? false ? 'required|' : null) . ($v['validation'] ?? ''),
                            $v['extra']['key'] => ($v['extra']['required'] ?? false ? 'required|' : null)
                                . ($v['extra']['validation'] ?? '')
                        ];
                    }
                    return [$k => ($v['required'] ?? false ? 'required|' : null) . ($v['validation'] ?? '')];
                })
                ->toArray()
        );

        $user = auth()->user();
        $user->unguard();

        $keys = [];
        $mainKeys = [];
        foreach (config('sso.public.custom_fields') as $key => $field) {
            if (
                ($field['extra']['invoke_value'] ?? false)
                && \request()->has($key)
                && (\request()->get($key) === $field['extra']['invoke_value'])
                && ($field['extra'] ?? false)
            ) {
                if ($field['extra']['main'] ?? false) {
                    $mainKeys[] = $field['extra']['key'];
                } else {
                    $keys[] = $field['extra']['key'];
                    continue;
                }
            }

            if ($field['main'] ?? false) {
                $mainKeys[] = $key;
                continue;
            }

            $keys[] = $key;
        }

        $user->update($request->only($keys));
        $user->updateOrCreateSSO([
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
            ] + $request->only($mainKeys));
    }
}
