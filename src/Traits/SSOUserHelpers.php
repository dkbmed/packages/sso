<?php

namespace DKBmed\SSO\Traits;

trait SSOUserHelpers
{
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getProfessionNameAttribute()
    {
        $profession_title = $this->profession->title;

        return $this->full_name . (($profession_title != "Other" && $profession_title != "N/A") ? ", " . $profession_title : "");
    }

    public function getTitleNameAttribute()
    {
        return "{$this->full_name}, {$this->title}";
    }
}