<?php

namespace DKBmed\SSO\Traits;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Schema;

trait SSOUser
{
    use SSOUserHelpers;

    public $remember_token = false;

    private $sso_user;
    private static $cache_ttl = 30;
    private static $guzzle;
    private static $host;

    public static function bootSSOUser()
    {
        self::$host = config('sso.api_host');
        self::$guzzle = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest'
            ]
        ]);

        static::retrieved(function ($user) {
            $user->populate();
        });
    }

    public function __get($key)
    {
        if (
            $key !== 'id'
            && $this->sso_user
            && property_exists($this->sso_user, $key)
            && !$this->hasAttribute($key)
        ) {
            return $this->sso_user->{$key};
        }
        return parent::__get($key);
    }

    // ACTIVE METHODS

    public function populate($populateAttributes = false)
    {
        $data = cache()->remember('sso_user_' . $this->sso_id, self::$cache_ttl, function () {
            try {
                $response = self::$guzzle->request(
                    'GET',
                    self::$host . '/api/user/' . $this->sso_id
                );

            } catch (GuzzleException $e) {
                return null;
            }
            return (string)$response->getBody();
        });

        $this->sso_user = json_decode($data);
        if ($populateAttributes) {
            $this->attributes = array_merge(json_decode($data, true) ?? [], $this->attributes);
        }

        return $this;
    }

    public static function updateOrCreateSSO($attributes)
    {
        if (isset($attributes['image'])) {
            $attributes['image'] = base64_encode(file_get_contents($attributes['image']));
        }

        try {
            $response = self::$guzzle->request(
                'POST',
                self::$host . '/api/user',
                [
                    'form_params' => $attributes
                ]
            );
        } catch (GuzzleException $e) {
            dd(self::$host . '/api/user', $e);
        }

        //TODO: maybe cache this
        $sso_user = json_decode((string)$response->getBody());
        cache()->delete('sso_user_' . $sso_user->id);
        return $sso_user;
    }

    public function isCustomDataRequired()
    {
        foreach (config('sso.public.custom_fields') as $key => $field) {
            if (
                ($field['required'] ?? false)
                && (Schema::hasColumn($this->getTable(), $key) || ($field['main'] ?? false))
                && $this->$key === null
            ) {
                return true;
            }
        }
        return false;
    }

    public function customData()
    {
        $model = $this;
        $fields = config('sso.public.custom_fields');
        $result = collect([]);

        array_walk($fields, function ($field, $key) use (&$result, $model) {
            $result[$key] = $model->$key;
            if ($field['extra'] ?? false) {
                $extraKey = $field['extra']['key'];
                $result[$extraKey] = $model->{$field['extra']['key']};
            }
        });

        return $result;
    }

    // HELPERS

    public static function ssoIdByEmail($email)
    {
        try {
            if (!self::$guzzle) {
                self::bootSSOUser();
            }
            
            $response = self::$guzzle->request(
                'GET',
                self::$host . '/api/user/email/' . $email
            );

        } catch (GuzzleException $e) {
            return null;
        }

        return json_decode((string)$response->getBody());
    }

    public function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributes);
    }
}
