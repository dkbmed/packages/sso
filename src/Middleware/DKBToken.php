<?php

namespace DKBmed\SSO\Middleware;

use Closure;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;

class DKBToken
{
    public function handle($request, Closure $next)
    {
        $host = config('sso.api_host');
        //TODO: cache this if token present and request time <= 60s or smth

        $is_api = $request->wantsJson();
        $token = $is_api ? $request->bearerToken() : $request->cookie('dkb_token');

        if ($token) {
            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request(
                    'GET',
                    $host . '/api/auth/me',
                    [
                        'headers' => [
                            'X-Requested-With' => 'XMLHttpRequest',
                            'Authorization' => 'Bearer ' . $token
                        ]
                    ]);
            } catch (GuzzleException $e) {
                if ($is_api) {
                    return abort('401', 'Token expired or invalid');
                }

                return redirect(config('sso.public.redirect', '/'))->withCookie(
                    cookie('dkb_token', null, -1),
                    cookie('dkb_custom_data', null, -1)
                );
            }

            $sso_user = json_decode($response->getBody());
            $user = config('sso.user_model')::firstOrCreate(
                [config('sso.column', 'sso_id') => $sso_user->id],
                [config('sso.column', 'sso_id') => $sso_user->id, 'role' => 'user']
            );
            $user->populate();

            if ($user->isCustomDataRequired() && !$is_api) {
                \Cookie::queue('dkb_custom_data', $user->customData(), 8400, null, null, false, false);
            } else {
                \Cookie::forget('dkb_custom_data');
            }

            Auth::login($user);
        } else {
            Auth::logout();
        }

        return $next($request);
    }
}
