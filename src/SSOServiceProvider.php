<?php

namespace DKBmed\SSO;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Cookie\Middleware\EncryptCookies;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->resolving(EncryptCookies::class, function ($object) {
            $object->disableFor(['dkb_token', 'dkb_custom_data']);
        });

        $kernel = $this->app->make(\Illuminate\Contracts\Http\Kernel::class);
        $kernel->pushMiddleware(\DKBmed\SSO\Middleware\DKBToken::class);

        $this->app['router']
            ->post('/sso/custom', 'DKBmed\SSO\Controllers\SSOController@storeCustomData');

        $this->loadViewsFrom(__DIR__ . '/templates', 'sso');

        $this->publishes([
            __DIR__ . '/../config/sso.php' => config_path('sso.php')
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__ . '/../config/sso.php', 'sso'
        );

        Blade::directive('sso_inject', function () {
            return '<script>!function(){window.dkbsso_config=' . json_encode(config('sso.public'))
                . ';var d=document.createElement("script");d.src="' . config('sso.api_host') . '/js/app.js?hash='
                . md5(date('Ymd')) . '",document.body.appendChild(d)}();</script>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
